#!/bin/bash

set -e

if [[ -e .env ]]; then
  source .env
fi

ssh_escape() {
  for arg; do
    printf '%q ' "$arg"
  done
}

if [[ -n "$SSH_TARGET_HOST" ]]; then
  echo "Using remote mode!"
  exec ./scripts/ssh \
    "export" "GITLAB_RAILS_REVISION=$GITLAB_RAILS_REVISION" "&&" \
    "export" "GITLAB_WORKHORSE_REVISION=$GITLAB_WORKHORSE_REVISION" "&&" \
    "export" "GITLAB_GITALY_REVISION=$GITLAB_GITALY_REVISION" "&&" \
    "export" "GITLAB_PAGES_REVISION=$GITLAB_PAGES_REVISION" "&&" \
    "export" "GITLAB_SHELL_REVISION=$GITLAB_SHELL_REVISION" "&&" \
    "export" "COMPOSE_KIT_REVISION=$COMPOSE_KIT_REVISION" "&&" \
    "export" "USE_RAILS_SERVER=$USE_RAILS_SERVER" "&&" \
    "mkdir" "-p" "$SSH_TARGET_DIR" "&&" \
    "cd" "$SSH_TARGET_DIR" "&&" \
    ./scripts/proxy $(ssh_escape "$@")
fi

if [[ -z "$HOST" ]]; then
  case "$(uname -s)" in
    Linux)
      export HOST=$(hostname -I | cut -d' ' -f1)
      ;;

    Darwin)
      export HOST=$(ipconfig getifaddr en0 || ipconfig getifaddr en1 || ipconfig getifaddr en2)
      ;;
  esac
fi

if [[ -z "$HOST" ]]; then
  echo "Could not detect IP address. Consider setting this as an environment variable: export HOST=<my-ip-address>"
  exit 1
fi

export CUSTOM_CONFIG=$(cat gitlab.yml)
export CUSTOM_UID=$(id -u)
export CUSTOM_GID=$(id -g)

exec "$@"
